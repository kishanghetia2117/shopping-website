import React, { Component } from 'react';
import axios from 'axios';
class AddProduct extends Component {
  constructor(props) {
    super(props)
    this.state = {
      category: '',
      description: '',
      image: '',
      price: '',
      // rating: {
      //   count: '',
      //   rate: '',
      // },
      title: ''
    }
  }
  changeHandler = (e) => {
    // if (e.target.name === 'count' || e.target.name === 'rate') {
    //   const tempObj = this.state.rating
    //   tempObj[e.target.name] = e.target.value
    //   this.setState({ rating: { ...tempObj } })
    // }
    // else {
    this.setState({ [e.target.name]: e.target.value });
    // }
  }
  submitHandler = e => {
    e.preventDefault()
    axios.post('https://fakestoreapi.com/products', this.state)
      .then(response => {
        this.props.updateProduct(response.data)
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    const { category, description, image, price, title, } = this.state;
    // const { count, rate } = this.state.rating;

    return <div className="d-flex justify-content-center">
      <button type="button" className="btn btn-primary mx-auto" data-bs-toggle="modal" data-bs-target="#exampleModal">
        Launch demo modal
      </button>
      <div className="modal fade" id="exampleModal" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <form onSubmit={this.submitHandler}>
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Add a Product</h5>
                <button type="button" className="btn-close" data-bs-dismiss="modal"></button>
              </div>
              <div className="modal-body">
                <div className="mb-3">
                  <label className="form-label">Product Name</label>
                  <input type="text" name="title" value={title} className="form-control" onChange={this.changeHandler} />
                </div>
                <div className="mb-3">
                  <label className="form-label">Product Discription</label>
                  <input type="text" name="description" value={description} className="form-control" onChange={this.changeHandler} />
                </div>
                <div className="mb-3">
                  <label className="form-label">Product Category</label>
                  <input type="text" name="category" value={category} className="form-control" onChange={this.changeHandler} />
                </div>
                <div className="mb-3">
                  <label className="form-label">Product Image URL</label>
                  <input type="text" name="image" value={image} className="form-control" onChange={this.changeHandler} />
                </div>
                <div className="mb-3">
                  <label className="form-label">Product Price</label>
                  <input type="number" name="price" value={price} className="form-control" onChange={this.changeHandler} placeholder="$0.0" />
                </div>
                {/* <div className="mb-3">
                  <label className="form-label">Product Star</label>
                  <input type="text" name="rate" value={rate} className="form-control" onChange={this.changeHandler} />
                </div>
                <div className="mb-3">
                  <label className="form-label">Rating People Count</label>
                  <input type="text" name="count" value={count} className="form-control" onChange={this.changeHandler} placeholder="0" />
                </div> */}
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" className="btn btn-primary">Add/Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  }
}

export default AddProduct;


