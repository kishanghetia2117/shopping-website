import React from 'react';
import './ProductCard.css';

function ProductCard(props) {
  const { product } = props;
  console.log(product);
  return <div className="col border-r">
    <div className="card border-r">
      <img className=" img-fluid card-img-top " src={product.image} alt="..." />
      <div className="card-body">
        <h5 className="card-title">{product.title}</h5>
        <small className="card-text ">{product.category}</small>
        <p className="card-text mt-4">{product.description}</p>
      </div>
      <div className="card-footer d-flex justify-content-between align-items-center p-4 border-bottom-r">
        {/* <small className="text-muted">{product.category}</small> */}
        <div className="d-flex align-items-start">
          <small>{product.rating.rate}</small>
          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-star-fill text-warning ms-2" viewBox="0 0 16 16" >
            <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"></path>
          </svg>
          <small className="ms-2">{'Rated by ' + product.rating.count + ' people'}</small>
        </div>
        <small className="fw-bold">{'$' + product.price}</small>
        <button type="button" className="btn btn-outline-primary">+Cart</button>
      </div>
    </div>
  </div>
}

export default ProductCard;
