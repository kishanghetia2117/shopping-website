import React, { Component } from 'react';
import axios from 'axios';
import Spinner from './Spinner'
import ProductContainer from './ProductContainer'
import AddProduct from './AddProduct'


class Product extends Component {
  constructor(props) {
    super(props)

    this.state = {
      productList: [],
      loadingData: true,
    }
  }

  componentDidMount() {
    axios.get('https://fakestoreapi.com/products')
      .then(res => {
        const productList = res.data
        this.setState({
          productList: productList,
          loadingData: false
        });
      })
      .catch(error => {
        this.setState({
          productList: false,
          loadingData: false
        });
      })
  }
  updateProduct = (product) => {
    product['rating'] = {}
    product['rating']['rate'] = '0'
    product['rating']['count'] = '0'
    // console.log(product)
    // console.log(this.state.productList, product)
    let newProductList = [...this.state.productList, product]
    this.setState({ productList: newProductList });
  }

  render() {
    console.log(this.state.productList)
    const { loadingData, productList } = this.state;
    if (loadingData) {
      return <Spinner />
    }
    else {
      if (productList === false) {
        return <div>Error loading data</div>
      } else if (productList.length === 0) {
        return <div>No products Found</div>
      } else {
        return <>
          <ProductContainer productList={productList}></ProductContainer>
          <div className="mt-5">
            <AddProduct updateProduct={this.updateProduct} />
          </div>
        </>
      }
    }
  }
}

export default Product;
