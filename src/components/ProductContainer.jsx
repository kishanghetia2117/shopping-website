import React from 'react';
import ProductCard from './ProductCard'

function ProductContainer(props) {
  const { productList } = props;
  return <div className="container p-4 mx-auto">
    <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
      {productList.map((product) => <ProductCard key={product.id} product={product} />)}
    </div>
  </div>;
}

export default ProductContainer;
