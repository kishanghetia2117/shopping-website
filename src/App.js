import './App.css';
// import 'bootstrap/dist/css/bootstrap.min.css';
import Product from './components/Product';
import NavBar from './components/NavBar';
import Footer from './components/Footer';


function App() {
  return (
    <>
      <NavBar />
      <Product />
      <Footer />
    </>
  );
}

export default App;
