import React from 'react';

function Footer() {
  return <footer className="pt-4 mt-5 pt-md-5 border-top bg-dark text-light">
    <div className="row container mx-auto">
      <div className="col-12 col-md">
        <a className="navbar-brand mb-2" href="#">Shoppers Stop</a>
        <small className="d-block mb-3 text-muted">© 2017–2021</small>
      </div>
      <div className="col-4 col-md">
        <h5>Features</h5>
        <ul className="list-unstyled text-small">
          <li className="mb-1"><a className="link-secondary text-decoration-none" href="#">1 day Delivery</a></li>
          <li className="mb-1"><a className="link-secondary text-decoration-none" href="#">Money Back</a></li>
          <li className="mb-1"><a className="link-secondary text-decoration-none" href="#">24/7 Support</a></li>
        </ul>
      </div>
      <div className="col-4 col-md">
        <h5>Resources</h5>
        <ul className="list-unstyled text-small">
          <li className="mb-1"><a className="link-secondary text-decoration-none" href="#">Track your order</a></li>
          <li className="mb-1"><a className="link-secondary text-decoration-none" href="#">History</a></li>
          <li className="mb-1"><a className="link-secondary text-decoration-none" href="#">Coupan offers</a></li>
        </ul>
      </div>
      <div className="col-4 col-md">
        <h5>About</h5>
        <ul className="list-unstyled text-small">
          <li className="mb-1"><a className="link-secondary text-decoration-none" href="#">Team</a></li>
          <li className="mb-1"><a className="link-secondary text-decoration-none" href="#">career</a></li>
          <li className="mb-1"><a className="link-secondary text-decoration-none" href="#">Privacy</a></li>
        </ul>
      </div>
    </div>
  </footer>
}

export default Footer;
